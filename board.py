import serial


class Board():
    def __init__(self, port):
        self.dev = serial.Serial(port)

    def digitalwrite(self, pin, data):
        print 'DIGITAL WRITE PIN: ', pin, ' DATA:', data

    def digitalread(self, pin):
        print 'DIGITAL READ PIN:', pin
